const postingsRouter = require('express').Router()
const config = require('../utils/config')
const rp = require('request-promise')
const JobCollection = require('../models/posting')

postingsRouter.get('/', async (request, response) => {
  const allPostings = await JobCollection.find({})
  response.json(allPostings.map(posting => posting.toJSON()))
})

postingsRouter.get('/id/:id', async (request, response, next) => {
  try {
    const posting = await JobCollection.findById(request.params.id)
    if (posting) {
      response.json(posting.toJSON())
    } else {
      response.status(404).end()
    }
  } catch (exception) {
    next(exception)
  }
})

postingsRouter.post('/query-search', async (request, response, next) => {
  try {
    const query = request.body.query
    const scrapeOptions = {
      body: {
        'query': query
      },
      json: true,
      uri: config.SCRAPER_API + 'scraper/api/all',
    }
    await rp.post(scrapeOptions)
    const allRes = await JobCollection.find({
      'tags': query
    })
    if (allRes) {
      response.json(allRes.map(posting => posting.toJSON()))
    } else {
      response.status(404).end()
    }
  } catch (exception) {
    next(exception)
  }
})

postingsRouter.put('/id/:id', async (request, response, next) => {
  const body = request.body

  const posting = {
    salary_numbers: body.salary_numbers,
    tags: body.tags,
    title: body.title,
    link: body.link,
    company: body.company,
    location: body.location,
    start_date: body.start_date,
    end_date: body.end_date,
    source: body.source,
    liked: body.liked,
    add_date: body.add_date,
    query: body.query
  }
  try {
    const updatedPosting = await JobCollection.findByIdAndUpdate(request.params.id, posting, {
      new: true
    })
    response.json(updatedPosting.toJSON())
  } catch (exception) {
    next(exception)
  }

})

module.exports = postingsRouter