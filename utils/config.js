require('dotenv').config()

let PORT = process.env.PORT
let MONGODB_JOBAPP = process.env.MONGODB_JOBAPP
let SCRAPER_API = process.env.SCRAPER_API

module.exports = {
  MONGODB_JOBAPP,
  PORT,
  SCRAPER_API
}