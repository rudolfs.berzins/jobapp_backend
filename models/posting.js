const mongoose = require('mongoose')

mongoose.set('useFindAndModify', false)

const defaultSchema = {
  title: String,
  link: String,
  salary_numbers: Array,
  company: String,
  location: String,
  start_date: String,
  end_date: String,
  source: String,
  liked: Boolean,
  add_date: Date,
  query: String,
  tags: Array
}

const JobCollectionSchema = new mongoose.Schema(defaultSchema, {
  collection: 'Job_Collection'
})

JobCollectionSchema.set('toJSON', {
  transform: (document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString()
    delete returnedObject._id
    delete returnedObject.__v
  }
})


module.exports = mongoose.model('Job_Collection', JobCollectionSchema)